from typing import Any

class File:
    def __init__(self) -> None:
        pass
    
    @staticmethod
    def create_file(self, _name:str, _purpose:str = "assistants")->Any:
        file:Any = self.client.files.create(
            file = open(_name, "rb"),
            purpose=_purpose
        )
        return file
            