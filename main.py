from assistant import Assistant
from termcolor import colored

_assistant = Assistant(
    "Sales", "instructions.txt", "description.txt", [], "gpt-3.5-turbo"
)
# _assistant.create_assistant()
# thread_id = _assistant.create_thread().id

# print(f"Assistant_id: {_assistant.assistant_id}, thread_id: {thread_id}")

thread_id = "thread_IhSnI1ihTbh82BGnk1p9WJZ4"
assistant_id = "asst_4MsueAx6A2ON1zIhcUoduXE0"

if __name__ == "__main__":
    while True:
        prompt = input(colored("Enter Your prompt", "green"))
        thread_message = _assistant.create_message(thread_id, prompt)
        print("Thread Message: {}".format(thread_message))
        run = _assistant.run_thread(thread_id, assistant_id)
        print(f"Run: {run}")
        
        assistant_response = None
        while not assistant_response:
            assistant_response = _assistant.get_assistant_message(thread_id, run.id)
        
        print(f"Assistant Response: {assistant_response}")