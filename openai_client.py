from dotenv import load_dotenv
from openai import OpenAI
import os


class OpenAiClient:
    def __init__(self) -> None:
        load_dotenv()
        _api_token: str = os.getenv("OPENAI_API_KEY")
        self.OpenAI = OpenAI(api_key=_api_token)
