from typing import List, Any
from file import File
from openai_client import OpenAiClient
from termcolor import colored


class Assistant:
    def __init__(
        self,
        _name: str,
        _instructions: str,
        _description: str,
        _file_ids: List[str],
        _model: str = "gpt-4-turbo-preview",
        _tools: str = "retrieval",
    ):
        self.name = _name
        self.instructions = _instructions
        self.description = _description
        self.file_ids = _file_ids
        self.model = _model
        self.tools = _tools
        self.client = OpenAiClient()

    def create_assistant(self) -> str:
        files_ids: List[str] = []
        file_instance: File = File()

        for file in self.file_ids:
            try:
                f = file_instance.create_file(file)
                files_ids.append(f.id)
            except Exception as e:
                raise RuntimeError("File Creation Error: {}".format(e))

        _instructions: str = Assistant.create_instructions(self.instructions)
        _description: str = Assistant.create_description(self.description)
        try:
            assistant = self.client.OpenAI.beta.assistants.create(
                name=self.name,
                model=self.model,
                instructions=_instructions,
                description=_description,
                file_ids=files_ids,
                tools=[{"type": self.tools}],
            )
            self.assistant_id = assistant.id
        except Exception as e:
            raise RuntimeError("Creation Assistant Error: {}".format(e))

    def creat_client_instance(self) -> None:
        try:
            self.client = OpenAiClient()
        except Exception as e:
            raise RuntimeError("openai client Error : {}".format(e))

    @staticmethod
    def create_instructions(instructions: str) -> str:
        with open(instructions, "r") as instruction_file:
            instructions: str = instruction_file.read()
        return instructions

    @staticmethod
    def create_description(description: str) -> str:
        with open(description, "r") as description_file:
            description: str = description_file.read()
        return description

    def create_thread(self) -> Any:
        thread = self.client.OpenAI.beta.threads.create()
        return thread

    def create_message(self, thread_id: str, message: str) -> Any:
        thread_message = self.client.OpenAI.beta.threads.messages.create(
            thread_id, role="user", content=message
        )
        return thread_message

    def run_thread(self, thread_id: str, assistant_id: str) -> None:
        print("Run Assistant Thread:")
        runed_thread = self.client.OpenAI.beta.threads.runs.create(
            thread_id=thread_id,
            assistant_id=assistant_id,
            instructions=self.instructions,
        )
        return runed_thread

    def run_steps(self, thread_id: str, run_id: str):
        run_steps = self.client.OpenAI.beta.threads.runs.steps.list(
            thread_id=thread_id, run_id=run_id
        )
        return run_steps

    def get_assistant_message(self, thread_id: str, run_id: str):
        try:
            run_steps = self.client.OpenAI.beta.threads.runs.steps.list(
                thread_id=thread_id, run_id=run_id
            )
            if not run_steps.data:
                return None
            if (
                run_steps.data[0].status == "completed"
                and run_steps.data[0].type == "message_creation"
            ):
                message_id: str = run_steps.data[
                    0
                ].step_details.message_creation.message_id
                message: str = self.get_message_by_id(message_id, thread_id)
                return message
        except Exception as e:
            raise SystemError(e)

    def get_message_by_id(self, message_id: str, thread_id: str):
        try:
            message_response:str = self.client.OpenAI.beta.threads.messages.retrieve(
                message_id=message_id, thread_id=thread_id
            )
            return message_response.content[0].text.value
        except Exception as e:
            raise SystemError(e)
